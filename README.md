[![Header](https://github.com/yaelleC/yaelleC/blob/main/yaelle_chaudy.png?raw=true "Header")](https://www.linkedin.com/in/yaellechaudy/)

# Hi all! 👋

Nice to meet you! I am Yaëlle, software engineer and researcher. After a PhD in Serious Games (building an [assessment engine and learning analytics tool for educators](https://www.researchgate.net/publication/305215768_An_Assessment_and_Learning_Analytics_Engine_for_Games-based_Learning)) and a postdoc in e-health (I worked on rules editors / authoring tools for clinicians to define rules for risk assessment and diagnostics to go alongside e-health apps) I have now joined the world of industry and work as a software engineer for [Skyscanner](https://www.skyscanner.net/). 

I have a passion for data and experimentation. 

I have personal projects mostly in the fields of photos, education & parenting.

The repositories for my PhD and Postdoc work are hosted on: https://github.com/yaelleUWS 

I also have some repos hosted on GitHub : https://github.com/yaelleC/

## 🔭 I’m currently working on ...

* A photodiary app to generate monthly/yearly journals of important pictures to share with family and print for home (currently hosted on bitbucket, will move and update a link later on)
* A reading app for parents to record books for their children (design stage, details to come later)

## 🌱 I’m currently learning ...

* UX Design on [Coursera](https://www.coursera.org/learn/ux-design-concept-wireframe/home/welcome)
* Mobile app development with React Native [here](https://www.coursera.org/learn/react-native/home/welcome) and [there](https://www.coursera.org/learn/android-app/home/welcome)
* Python for Data Science on [Udemy](https://www.udemy.com/course/python-for-data-science-and-machine-learning-bootcamp/)

## 💬 Ask me about ...

* Experimentation at scale
* Monitoring / Alerting
* Web development
* TDD
* Doing a PhD ;) 
